#include <stdio.h>

int example(int a, int b, int m, int n) {
    int x, y, z, t;
    z = m + n;
    if (a) {
        x = m;
        y = n;
    } else {
        x = n;
        y = m;
    }
    if (x + y >= m + n) {
    } else {
        // dead code
        for (int i = 0; i < b; i++) {
            z *= i;
        }
    }
    z += (x + y);
    return z;
}

int main() {
    int a, b, m, n;
    scanf("%d", &a);
    scanf("%d", &b);
    scanf("%d", &m);
    scanf("%d", &n);
    return example(a, b, m, n);
}

/**
 * LLVM 不能够识别出x + y是一个定值
 * GCC 能够识别并消除
*/