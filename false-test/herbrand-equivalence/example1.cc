#include <stdio.h>

int input(int a, int b) {
    int c, d, e, x, y, z;
    scanf("%d", &d);
    if(d) {
        x = a + 1;
        c = a;
    } else {
        x = b + 1;
        c = b;
    }
    y = c + 1;
    scanf("%d", &e);
    if(e) {
        return x;
    } else {
        return y;
    }
}

int main()
{
    int a, b;
    scanf("%d %d", &a, &b);
    input(a, b);
}