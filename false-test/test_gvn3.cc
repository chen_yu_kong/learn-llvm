/**
 * the difference between this an test_gvn2 is this example more simple and direct.
*/
#include <stdio.h>
int input() {
    int x;
    scanf("%d", &x);
    return x;
}
int main() {
    int a = input(), b = input(), m = input(), n = input();
    int y, c;
    if(b) {
        c = m;
        y = n;
    } else {
        c = n;
        y = m;
    }
    return y + c;
}