#include <stdio.h>

int min(int x, int y) {
    return x < y ? x : y;
}


void swap(int &x, int &y) {
    int temp = x;
    x = y;
    y = temp;
}

int main() {
    int x, y;
    scanf("%d %d", &x, &y);
    int z = y;
    //cannot opt
    while(x < y) {
        z = min(x, y);
        swap(x, y);
    }
    //can opt
    if(x < y) {
        z = min(x, y);
        swap(x, y);
    }
    return z;

}