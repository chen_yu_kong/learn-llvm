/**
 * this example is test if LLVM can delete the equivalence between phi node.
*/
#include <stdio.h>
int input() {
    int x;
    scanf("%d", &x);
    return x;
}
int main() {
    int a = input(), b = input(), m = input(), n = input();
    int y, c;
    if(b) {
        c = m + a;
        y = m;
    } else {
        c = n + b;
        y = n;
    }
    return y - c;
}