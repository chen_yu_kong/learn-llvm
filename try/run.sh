cmake -B build
cmake --build build

/usr/bin/clang-14 -S -emit-llvm -O0 -Xclang -disable-O0-optnone tmp/foo.c -o tmp/foo.ll

/usr/bin/opt-14 -S -load-pass-plugin=build/libmypass.so -passes="mypass" tmp/foo.ll -o tmp/foo_mypass.ll
