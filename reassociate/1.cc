void foo(int a, int b, int c, int d, int e, int *res) {
  res[0] = (e * a) * d;
  res[1] = (e * b) * d;
  res[2] = (e * c) * d;
}

// int main() {
//   int a, b, c, d, e, res[10];
//   foo(a, b, c, d, e, res);
// }
