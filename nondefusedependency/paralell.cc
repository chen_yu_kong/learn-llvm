#include <iostream>
#include <omp.h>

int main() {
    int x = 0;
    #pragma omp parallel
    {
        #pragma omp for
        for(int i = 0; i < 100; ++i)
            x = x + 1;
    }
    std::cout << x << std::endl;
}
