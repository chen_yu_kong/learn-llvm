#include <limits.h>
int llvm(unsigned a, unsigned b) {
    if(a + b < UINT_MAX) {
        return a + b;
    }
}

/**
 *  
 * define dso_local noundef i32 @llvm(unsigned int, unsigned int)(i32 noundef %a, i32 noundef %b) local_unnamed_addr {
entry:
  %add = add i32 %b, %a
  %cmp = icmp ne i32 %add, -1
  tail call void @llvm.assume(i1 %cmp)
  ret i32 %add
}
 */

/***
 * 此处会出现assume是因为a+b可能为UINT_MAX，但此时没有返回函数，所以添加了assume，假设不存在不满足if条件的情况。
*/
