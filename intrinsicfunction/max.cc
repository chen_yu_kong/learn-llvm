int max(int a, int b)
{
    return a > b ? a : b;
}
//  @llvm.smax.i32

// 修改为unsigned可以得到umax